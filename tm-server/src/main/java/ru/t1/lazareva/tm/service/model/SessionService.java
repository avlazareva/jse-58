package ru.t1.lazareva.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.model.ISessionRepository;
import ru.t1.lazareva.tm.api.service.model.ISessionService;
import ru.t1.lazareva.tm.model.Session;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}
