package ru.t1.lazareva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.lazareva.tm.api.service.dto.ISessionDtoService;
import ru.t1.lazareva.tm.dto.model.SessionDto;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto, ISessionDtoRepository> implements ISessionDtoService {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    protected ISessionDtoRepository getRepository() {
        return context.getBean(ISessionDtoRepository.class);
    }

}