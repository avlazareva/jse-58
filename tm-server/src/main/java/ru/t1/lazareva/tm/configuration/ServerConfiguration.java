package ru.t1.lazareva.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.Session;
import ru.t1.lazareva.tm.model.Task;
import ru.t1.lazareva.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.t1.lazareva.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDBDriver());
        settings.put(Environment.URL, propertyService.getDBUrl() + propertyService.getDBSchema());
        settings.put(Environment.USER, propertyService.getDBUser());
        settings.put(Environment.PASS, propertyService.getDBPassword());
        settings.put(Environment.DIALECT, propertyService.getDBDialect());
        settings.put(Environment.FORMAT_SQL, "true");
        settings.put(Environment.SHOW_SQL, propertyService.getDBShowSQL());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2DDL());
        if ("true".equals(propertyService.getDBCache())) {
            settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBCache());
            settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBCacheRegion());
            settings.put(Environment.USE_QUERY_CACHE, propertyService.getDBQueryCache());
            settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBMinimalPuts());
            settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBCacheRegionPrefix());
            settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBCacheProvider());
        }
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDto.class);
        source.addAnnotatedClass(TaskDto.class);
        source.addAnnotatedClass(UserDto.class);
        source.addAnnotatedClass(SessionDto.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
