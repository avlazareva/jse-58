package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IUserEndpoint;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;
import ru.t1.lazareva.tm.exception.AbstractException;
import ru.t1.lazareva.tm.marker.SoapCategory;
import ru.t1.lazareva.tm.service.PropertyService;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @Nullable
    public final static String ADMIN_LOGIN = "admin";
    @Nullable
    public final static String ADMIN_PASSWORD = "admin";
    @Nullable
    public final static String USER_LOGIN = "test1";
    @Nullable
    public final static String USER_PASSWORD = "test1";
    @Nullable
    public final static String USER_REGISTRY_LOGIN = "REGISTRY";
    @Nullable
    public final static String USER_REGISTRY_PASSWORD = "REGISTRY";
    @Nullable
    public final static String USER_EMAIL = "USER_EMAIL1";
    @Nullable
    public final static String USER_REGISTRY_EMAIL = "USER_REGISTRY_EMAIL";
    @Nullable
    public final static String USER_FIRST_NAME = "USER_FIRST_NAME";
    @Nullable
    public final static String USER_MIDDLE_NAME = "USER_MIDDLE_NAME";
    @Nullable
    public final static String USER_LAST_NAME = "USER_LAST_NAME";
    @NotNull
    private final static IPropertyService PROPERTY_SERVICE = new PropertyService();
    @NotNull
    private final static IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static IUserEndpoint ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);
    @Nullable
    private static String ADMIN_TOKEN;
    @Nullable
    private static String USER_TOKEN;

    @BeforeClass
    public static void before() throws AbstractException {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = AUTH_ENDPOINT.login(loginAdminRequest);
        ADMIN_TOKEN = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        ENDPOINT.registryUser(request);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = AUTH_ENDPOINT.login(loginUserRequest);
        USER_TOKEN = loginUserResponse.getToken();
    }

    @AfterClass
    public static void after() throws AbstractException {
        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(USER_TOKEN);
        AUTH_ENDPOINT.logout(logoutUserRequest);
        USER_TOKEN = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeUserRequest.setLogin(USER_LOGIN);
        ENDPOINT.removeUser(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(logoutAdminRequest);
        ADMIN_TOKEN = null;
    }

    @Test
    public void changePassword() throws AbstractException {
        @NotNull final String newPassword = "newPassword";
        @NotNull final UserChangePasswordRequest userChangePasswordRequest = new UserChangePasswordRequest(USER_TOKEN);
        userChangePasswordRequest.setPassword(newPassword);
        @NotNull final UserChangePasswordResponse userChangePasswordResponse = ENDPOINT.changeUserPassword(userChangePasswordRequest);
        Assert.assertNotNull(userChangePasswordResponse);
        Assert.assertNotNull(userChangePasswordResponse.getUser());
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(USER_TOKEN);
        request.setPassword(USER_PASSWORD);
        ENDPOINT.changeUserPassword(request);
    }

    @Test
    public void lock() throws AbstractException {
        @NotNull final UserLockRequest userLockRequest = new UserLockRequest(ADMIN_TOKEN);
        userLockRequest.setLogin(USER_LOGIN);
        @NotNull final UserLockResponse response = ENDPOINT.lockUser(userLockRequest);
        Assert.assertNotNull(response);

        @NotNull final UserUnlockRequest userUnlockRequest = new UserUnlockRequest(ADMIN_TOKEN);
        userUnlockRequest.setLogin(USER_LOGIN);
        ENDPOINT.unlockUser(userUnlockRequest);
    }

    @Test
    public void registry() throws AbstractException {
        @NotNull final UserRegistryResponse response = ENDPOINT.registryUser(new UserRegistryRequest(USER_REGISTRY_LOGIN, USER_REGISTRY_PASSWORD, USER_REGISTRY_EMAIL));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());

        @NotNull final UserRemoveRequest userRemoveRequest = new UserRemoveRequest(ADMIN_TOKEN);
        userRemoveRequest.setLogin(USER_REGISTRY_LOGIN);
        ENDPOINT.removeUser(userRemoveRequest);
    }

    @Test
    public void remove() throws AbstractException {
        ENDPOINT.registryUser(new UserRegistryRequest(USER_REGISTRY_LOGIN, USER_REGISTRY_PASSWORD, USER_REGISTRY_PASSWORD));

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(ADMIN_TOKEN);
        request.setLogin(USER_REGISTRY_LOGIN);
        @NotNull final UserRemoveResponse response = ENDPOINT.removeUser(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());
    }

    @Test
    public void unlock() throws AbstractException {
        @NotNull final UserLockRequest userLockRequest = new UserLockRequest(ADMIN_TOKEN);
        userLockRequest.setLogin(USER_LOGIN);
        ENDPOINT.lockUser(userLockRequest);

        @NotNull final UserUnlockRequest userUnlockRequest = new UserUnlockRequest(ADMIN_TOKEN);
        userUnlockRequest.setLogin(USER_LOGIN);
        @NotNull final UserUnlockResponse response = ENDPOINT.unlockUser(userUnlockRequest);
        Assert.assertNotNull(response);
    }

    @Test
    public void updateProfile() throws AbstractException {
        @NotNull final UserLockRequest userLockRequest = new UserLockRequest(ADMIN_TOKEN);
        userLockRequest.setLogin(USER_LOGIN);
        ENDPOINT.lockUser(userLockRequest);

        @NotNull final UserUpdateProfileRequest userUpdateProfileRequest = new UserUpdateProfileRequest(USER_TOKEN);
        userUpdateProfileRequest.setLastName(USER_LAST_NAME);
        userUpdateProfileRequest.setFirstName(USER_FIRST_NAME);
        userUpdateProfileRequest.setMiddleName(USER_MIDDLE_NAME);
        @NotNull final UserUpdateProfileResponse response = ENDPOINT.updateUserProfile(userUpdateProfileRequest);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());
    }

}
